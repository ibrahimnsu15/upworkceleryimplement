from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(SearchQuery)
admin.site.register(Job)
admin.site.register(Twitter)
