from django.apps import AppConfig


class scrapperWorkerConfig(AppConfig):
    name = 'apps.scrapper'
