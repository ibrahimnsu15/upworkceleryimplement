# Generated by Django 3.0.10 on 2021-01-04 18:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0003_auto_20210104_1703'),
    ]

    operations = [
        migrations.RenameField(
            model_name='twitter',
            old_name='userId',
            new_name='twitterId',
        ),
    ]
