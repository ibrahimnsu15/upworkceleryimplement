from django.db import models


class Twitter(models.Model):
    twitterId = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    url = models.CharField(max_length=255, )
    email = models.CharField(max_length=255, null=True, blank=True)
    query = models.CharField(max_length=255),
    created_by_id = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
