from celery import shared_task
from celery_progress.backend import ProgressRecorder
import time
from apps.jobs.models import SearchQuery, Job, Twitter
from threading import Timer
import requests
import csv
import tweepy
import apps.scrapperWorker.scrapper.Util as u
import apps.scrapperWorker.scrapper.Config as config
from urllib.parse import urlparse


@shared_task()
def celery_task(self,  user_id, query, excludedMail):
    print("Task added")
    print(user_id)
    api_key = config.api_key
    api_secret = config.api_secret
    access_token = config.access_token
    access_token_secret = config.access_token_secret

    auth = tweepy.OAuthHandler(api_key, api_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    query = query
    avoid = excludedMail
    save = True

    account_count = 0
    ids = []

    filter_list = u.getFilterList()

    keywords = query.split(",")
    query_key = query

    id_limit = 100
    id_counter = 0
    end_loop = False

    for keyword in keywords:
        accounts = tweepy.Cursor(api.search_users, keyword).pages(51)
        keyword = keyword.lstrip()

        for page in accounts:
            for account in page:
                id = account.id_str
                emails = []
                new_emails = []

                if id not in ids:
                    screen_name = account.screen_name

                    try:
                        r = requests.get(account.url)
                        new_url = r.url
                        url = "https://" + urlparse(new_url).netloc

                        try:
                            emails = u.emailExtractor(url)
                            validity = True

                            for email in emails:
                                email = str(email).replace("thello", "hello").replace("tsupport", "support").replace(
                                    "tinfo", "info")

                                for word in filter_list:
                                    if word in email:
                                        validity = False

                                if validity:
                                    email = email.lower()
                                    new_emails.append(email)

                            new_emails = list(dict.fromkeys(new_emails))

                        except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
                            # print("(" + keyword + " #" + str(
                            #     account_count) + "): " + id + " | " + screen_name + " | " + url)
                            continue

                        account_count += 1

                        # with open("Contacts.csv", "a", newline='') as contacts:
                        #     writer = csv.writer( contacts, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

                        if len(new_emails) > 0:
                            for email in new_emails:
                                Twitter.objects.create(
                                    twitterId=id,
                                    username=screen_name,
                                    url=url,
                                    email=email,
                                    query_key=query_key,
                                    created_by=user_id
                                )
                                id_counter += 1
                                print(email)

                        ids.append(id)
                        print("after ")
                        print(id_counter, id_limit, id_counter == id_limit)
                        if id_counter == id_limit:
                            return query_key

                    except:
                        pass

    return query_key
