from .stox import check, valid
from django.db.models import Q
from apps.scrapper.models import Job, Twitter
from .free import allowed
from apps.scrapper.tasks import celery_task
import json
import requests
from apps.users.forms import StockTicker, TwitterQueryForm
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
import logging
log = logging.getLogger(__name__)
# from django.views.decorators.csrf import csrf_protect
# from django.template import RequestContext


def home(request):
    if request.user.is_authenticated:

        return render(request, 'web/app_home.html', context={
            'active_tab': 'dashboard',
        })

    else:
        return render(request, 'web/landing_page.html')


def analyze(request):
    if request.method == 'POST':
        if allowed(request):
            form = StockTicker(request.POST)
            stock = request.POST['ticker']
            if form.is_valid():
                try:
                    valid(stock)
                    return render(request, 'web/analyzer.html', context={
                        'active_tab': 'dashboard', 'data': check(stock), 'form': form})
                except Exception as e:
                    capture_message(str(e), level="error")
                    messages.warning(request, 'Invalid Stock Ticker')
        if allowed(request) == False:
            messages.warning(
                request, 'Only 3 requests allowed per day. Please upgrade for unlimited access')
            return render(request, 'web/analyzer.html', context={'active_tab': 'dashboard', 'form': StockTicker()})
    return render(request, 'web/analyzer.html', context={'active_tab': 'dashboard', 'form': StockTicker()})


def scraper(request):
    user = request.user
    if request.method == 'POST':
        if allowed(request):
            query = request.POST.get('query')
            excludedMail = False
            if request.POST.get('excludedMail'):
                excludedMail = True
            celeryJob = celery_task.delay(user.pk, query, excludedMail)

            return render(request, 'web/scraper.html', context={'task_id': celeryJob.task_id})
            # else:
            #     print("Debug 3")
            #     # pass

        if allowed(request) == False:
            messages.warning(
                request, 'Only 3 requests allowed per day. Please upgrade for unlimited access')
    else:
        query_id = request.GET.get('queryApplied')
        user_filter = Q(created_by=user)

        if query_id:
            query_filter = Q(query_id=query_id)

        filters = query_filter & user_filter
        queryResult = Twitter.objects.filter(filters)

        return render(request, 'web/scraper.html', context={'active_tab': 'dashboard', 'queryResult': list(queryResult)})


def options(request):
    r = requests.get(
        "https://eodhistoricaldata.com/api/options/AAPL.US?api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX")
    data = json.loads(r.content)
    code = data["code"]
    options = data["data"]
    if data["lastTradePrice"] is None:
        price = valid(data["code"])
    else:
        price = data["lastTradePrice"]
    context = {'options': options, 'price': price, "code": code}
    return render(request, 'web/options.html', context=context)


def resources(request):
    return render(request, 'web/resources.html')
